//JQuery Module Pattern

// An object literal
var app = {
    init: function() {
      app.functionOne();
    },
    functionOne: function () {
    }
  };


  // $(window).scroll(function() {
  //   if ($(document).scrollTop() > 50) {
  //     $('nav').addClass('shrink');
  //   } else {
  //     $('nav').removeClass('shrink');
  //   }
  // });

  $(document).ready(function() {
    // Transition effect for navbar 
    $(window).scroll(function() {
      // checks if window is scrolled more than 500px, adds/removes solid class
      if($(this).scrollTop() > 200) { 
          $('.navbar').addClass('solid');
          $('nav').addClass('shrink');
      } else {
          $('.navbar').removeClass('solid');
          $('nav').removeClass('shrink');
      }
    });
});